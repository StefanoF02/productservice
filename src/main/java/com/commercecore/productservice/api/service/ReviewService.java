package com.commercecore.productservice.api.service;

import com.commercecore.productservice.api.mapper.ProductMapper;
import com.commercecore.productservice.api.mapper.ReviewMapper;
import com.commercecore.productservice.entity.Review;
import com.commercecore.productservice.entity.repository.ReviewRepository;
import lombok.AllArgsConstructor;
import model.ProductDTO;
import model.ReviewDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@AllArgsConstructor
@Service
public class ReviewService {

    @Autowired
    private final ReviewMapper reviewMapper;

    @Autowired
    private final ProductMapper productMapper;

    @Autowired
    private final ReviewRepository reviewRepository;

    public ReviewDTO addReviewToProduct(ProductDTO productDTO, ReviewDTO reviewDTO) {
        Review review = reviewMapper.toDomain(reviewDTO);
        review.setProduct(productMapper.toDomain(productDTO));
        review.setReviewDate(LocalDate.now());
        reviewRepository.save(review);
        return reviewDTO;
    }

}
