package com.commercecore.productservice.api.service;


import com.commercecore.productservice.api.mapper.ProductMapper;
import com.commercecore.productservice.entity.repository.ProductRepository;
import lombok.AllArgsConstructor;
import model.ProductDTO;
import model.ProductWithReviewsDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@Service
public class ProductService {

    @Autowired
    private final ProductRepository productRepository;

    @Autowired
    private final ProductMapper productMapper;

    public ProductDTO saveProduct(ProductDTO productDTO) {
        productRepository.save(productMapper.toDomain(productDTO));
        return productDTO;
    }

    public ProductDTO updateProduct(ProductDTO productDTO) {
        if(!productRepository.existsById(productMapper.toDomain(productDTO).getArticleNumber())){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                    "Product with article number " + productDTO.getArticleNumber() + " not found");
        }
        productRepository.save(productMapper.toDomain(productDTO));
        return productDTO;
    }

    public ProductDTO findProductByArtNumber(Long articleNumber) {
        return productMapper.productToApi( productRepository.findById(articleNumber).orElseThrow(
                () -> new ResponseStatusException(HttpStatus.NOT_FOUND,
                        "Product with article number " + articleNumber + " not found")));
    }

    public ProductWithReviewsDTO findProductWithReviewsByArtNumber(Long articleNumber) {
        return productMapper.productWithReviewtoApi( productRepository.findById(articleNumber).orElseThrow(
                () -> new ResponseStatusException(HttpStatus.NOT_FOUND,
                        "Product with article number " + articleNumber + " not found")));
    }

    public List<ProductWithReviewsDTO> findAllByProducer(String producer){
        List<ProductWithReviewsDTO> productsByProducer = new ArrayList<>();

        productRepository.findByProducer(producer)
                .forEach(product -> productsByProducer.add(productMapper.productWithReviewtoApi(product)));

        return productsByProducer;
    }

    public List<ProductWithReviewsDTO> findAllByCategories(String producer){
        List<ProductWithReviewsDTO> productsByProducer = new ArrayList<>();

        productRepository.findByCategories(producer)
                .forEach(product -> productsByProducer.add(productMapper.productWithReviewtoApi(product)));

        return productsByProducer;
    }

    public HttpStatus deleteProduct(Long articleNumber) {
        var product = productRepository.findById(articleNumber);
        if (productRepository.existsById(articleNumber)) {
            productRepository.deleteById(articleNumber);
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                    "Product with article number " + articleNumber + " not found");
        }
        return HttpStatus.NO_CONTENT;
    }
}
