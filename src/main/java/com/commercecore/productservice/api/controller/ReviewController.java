package com.commercecore.productservice.api.controller;


import api.ReviewsApi;
import com.commercecore.productservice.api.service.ProductService;
import com.commercecore.productservice.api.service.ReviewService;
import lombok.AllArgsConstructor;
import model.ProductDTO;
import model.ReviewDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;


@AllArgsConstructor
@RestController
public class ReviewController implements ReviewsApi {

    @Autowired
    private final ReviewService reviewService;

    @Autowired
    private final ProductService productService;

    @Override
    public ResponseEntity<ReviewDTO> addReviewToProduct(ReviewDTO reviewDTO) {
        ProductDTO productDTO = productService.findProductByArtNumber(reviewDTO.getArticleNumber());
        return new ResponseEntity<>(reviewService.addReviewToProduct(productDTO, reviewDTO), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ReviewDTO> getReviewsByProduct(Long articlenumber) {
        return ReviewsApi.super.getReviewsByProduct(articlenumber);
    }
}
