package com.commercecore.productservice.api.controller;

import api.ExternalApi;
import com.commercecore.productservice.api.service.ProductService;
import lombok.AllArgsConstructor;
import model.ProductWithReviewsDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@RestController
public class ExternalProductController implements ExternalApi {

    @Autowired
    private final ProductService productService;

    @Override
    public ResponseEntity<ProductWithReviewsDTO> getProductWithReviews(Long articleNumber) {
        ProductWithReviewsDTO product = productService.findProductWithReviewsByArtNumber(articleNumber);
        return new ResponseEntity<>(product, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<ProductWithReviewsDTO>> getMutlipleProductsByProducer(String producer) {
        List<ProductWithReviewsDTO> products = productService.findAllByProducer(producer);
        return new ResponseEntity<>(products, HttpStatus.OK);
    }
}

