package com.commercecore.productservice.api.controller;

import api.InternalApi;
import com.commercecore.productservice.api.service.ProductService;
import lombok.AllArgsConstructor;
import model.ProductDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@AllArgsConstructor
@RestController
public class InternalProductController implements InternalApi {

    @Autowired
    private final ProductService productService;


    @Override
    public ResponseEntity<ProductDTO> getProduct(Long articleNumber) {
        ProductDTO product = productService.findProductByArtNumber(articleNumber);
        return new ResponseEntity<>(product, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ProductDTO> createProduct(ProductDTO productDTO) {
        return new ResponseEntity<>(productService.saveProduct(productDTO),HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ProductDTO> updateProduct(ProductDTO productDTO) {
        return new ResponseEntity<>(productService.updateProduct(productDTO), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Void> deleteProduct(Long articleNumber) {
        return new ResponseEntity<>(productService.deleteProduct(articleNumber));
    }

}
