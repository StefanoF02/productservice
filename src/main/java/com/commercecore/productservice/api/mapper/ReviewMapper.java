package com.commercecore.productservice.api.mapper;

import com.commercecore.productservice.entity.Review;
import model.ReviewDTO;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ReviewMapper {

    ReviewDTO toApi(Review review);

    Review toDomain(ReviewDTO reviewDTO);

}
