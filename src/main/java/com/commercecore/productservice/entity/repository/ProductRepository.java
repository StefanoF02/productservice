package com.commercecore.productservice.entity.repository;

import com.commercecore.productservice.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProductRepository extends JpaRepository<Product,Long> {

    boolean existsById(Long articleNumber);

    <S extends Product> S save(S product);

    @Query("SELECT p FROM Product p LEFT JOIN FETCH p.reviews WHERE p.articleNumber = :articleNumber")
    Optional<Product> findById(@Param("articleNumber") Long articleNumber);

    List<Product> findByProducer(String producer);

    List<Product> findByCategories(String category);

    void deleteById(Long articleNumber);

}
