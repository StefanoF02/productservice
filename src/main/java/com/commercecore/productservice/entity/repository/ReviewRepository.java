package com.commercecore.productservice.entity.repository;

import com.commercecore.productservice.entity.Review;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ReviewRepository extends JpaRepository<Review,Integer> {

    <R extends Review> R save(R review);

    Optional<Review> findById(Integer id);

    void deleteById(Integer id);

}
