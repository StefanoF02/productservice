package com.commercecore.productservice.entity;

import jakarta.persistence.*;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import lombok.*;

import java.time.LocalDate;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "reviews")
public class Review {
    @Valid

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Integer id;

    @NotNull(message = "Product must be stated.")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "articleNumber")
    private Product product;

    @NotNull(message = "Rating must be set.")
    private Integer rating;

    private String comment;

    private LocalDate reviewDate;

}
