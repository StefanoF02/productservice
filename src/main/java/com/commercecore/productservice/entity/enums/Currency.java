package com.commercecore.productservice.entity.enums;

public enum Currency {
    EUR("EUR"),
    USD("USD"),
    AUD("AUD"),
    CAD("CAD"),
    YEN("YEN"),
    GBP("GBP"),
    CHF("CHF");

    public final String currency;

    private Currency(String currency){ this.currency = currency;};

}