package com.commercecore.productservice;

import com.commercecore.productservice.api.controller.ExternalProductController;
import com.commercecore.productservice.api.controller.InternalProductController;
import com.commercecore.productservice.api.controller.ReviewController;
import com.commercecore.productservice.api.service.ProductService;
import com.commercecore.productservice.api.service.ReviewService;
import com.commercecore.productservice.entity.repository.ProductRepository;
import com.commercecore.productservice.entity.repository.ReviewRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
class ProductserviceApplicationTests {

    @Autowired
    private ExternalProductController externalProductController;

    @Autowired
    private InternalProductController internalProductController;

    @Autowired
    private ProductService productService;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private ReviewController reviewController;

    @Autowired
    private ReviewService reviewService;

    @Autowired
    private ReviewRepository reviewRepository;


//    @Test
//	void contextLoads() throws Exception {
//        assertNotNull(externalProductController);
//        assertNotNull(internalProductController);
//        assertNotNull(productService);
//        assertNotNull(productRepository);
//        assertNotNull(reviewController);
//        assertNotNull(reviewService);
//        assertNotNull(reviewRepository);
//	}

}
